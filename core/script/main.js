            //====================//
            //==== Variables ====//
            //==================//
const homeContainer = document.querySelector("#home");
const finishContainer = document.querySelector("#result");
const questionContainer = document.querySelector("#question");

        //==================================//
        //===== Button Start Function =====//
        //================================//
let buttonStart = document.querySelector(".btn-start");

buttonStart.addEventListener("click", function(){
    startQuestion();
})


function startQuestion() {
    let errorMessage = document.querySelector(".error-name")


    if (userName.value.length == 0) {
        errorMessage.innerHTML = `without username we cannot play the quiz!`;

    } else if (userName.value.length <= 3 ) {
        errorMessage.innerHTML = `your username must be longer than 3 characters!`;

    } else if  (userName.value.length >= 12 ) {
        errorMessage.innerHTML = `your username must be shorter than 12 characters!`;

    } else {
        homeContainer.style.display = "none";
        questionContainer.style.display = "flex";
    }
    
}

//===============================//
//======JSON Static Object======//
//=============================//
let allquestion = [
    {   
        number: "1",
        question: "Javascript programming language is an _______ language?",
        answer: {
            a: " A.  Procedural",
            b: " B.  Object-Based",
            c: " C.  Object-Oriented"
            },
            correctAnswer: "c"
        },
    {
        number: "2",
        question: "How can a datatype be declared to be a constant type?",
        answer: {
            a: " A.  var",
            b: " B.  const",
            c: " C.  let"
            },
            correctAnswer: "b"
        },
    {
        number: "3",
        question: "Which of the following keywords is used to define a variable in Javascript?",
        answer: {
            a: " A.  var",
            b: " B.  let",
            c: " C.  Both A and B",
            },
            correctAnswer: "c"
        },
    {
        number: "4",
        question: "When an operator's value is NULL, the typeof returned by the unary operator is:",
        answer: {
            a: " A.  Boolean",
            b: " B.  Object",
            c: " C.  Integer"
            },
            correctAnswer: "b"
        },
    {
        number: "5",
        question: "Which function is used to serialize an object into a JSON string in Javascript?",
        answer: {
            a: " A.  stringify()",
            b: " B.  parse()",
            c: " C.  convert()"
            },
            correctAnswer: "a"
        },
    {
        number: "6",
        question: "What keyword is used to declare an asynchronous function in Javascript?",
        answer: {
            a: " A.  await",
            b: " B.  setTimeout",
            c: " C.  async"
            },
            correctAnswer: "c"
        },
    {
        number: "7",
        question: "In Javascript programming language the function and var are known as:",
        answer: {
            a: " A.  Keywords",
            b: " B.  Data types",
            c: " C.  Declaration statements"
            },
            correctAnswer: "c"
        },
    {
        number: "8",
        question: "Which one of the following is an ternary operator:",
        answer: {
            a: " A.   - ",
            b: " B.   + ",
            c: " C.   ? "
            },
            correctAnswer: "c"
        },
    {
        number: "9",
        question: "In which HTML element, we put the JavaScript code?",
        answer: {
            a: " A.  javascript tag",
            b: " B.  script tag",
            c: " C.  js tag"
            },
            correctAnswer: "b"
        },
    {
        number: "10",
        question: "Which JavaScript method is used to access an HTML element by id?",
        answer: {
            a: " A.  getElementByTagName",
            b: " B.  getElementByClassName",
            c: " C.  getElementById"
            },
            correctAnswer: "c"
        },
];

//===================================//
//=========Looping question=========//
//=================================//
let questionHtml = "";

allquestion.forEach((question, index) => {
    if (index === 0){
        questionHtml += `
        <div id="list" class="question-page">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}</h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a}</label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b}</label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c}</label>
                </div>
                <div class="question-page__question--button">
                    <button id="next" class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else if (index <= 8 ){
        questionHtml += `
        <div id="list" class="question-page" style="display:none">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}</h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a}</label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b}</label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c}</label>
                </div>
                <div class="question-page__question--button">
                    <button id="prev" class="btn-prev">Prev</button>
                    <button id="next" class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else {
        questionHtml += `
        <div id="list" class="question-page" style="display:none">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}</h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a}</label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b}</label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c}</label>
                </div>
                <div class="question-page__question--button">
                    <button id="prev" class="btn-prev">Prev</button>
                    <button id="finish" class="btn-finish">Finish</button>
                </div>
            </div>

            <div id="my-modal" class="modal">
                <div class="modal__content">
                    <h2 class="modal__content--title">REMINDER!</h2>
                    <p class="modal__content--attention">"You can't finish the quiz before you answer all question!"</p>
                    <button id="close-btn" class="close-btn">X</button>
                </div>
            </div>
        </div>`
    }
});

questionContainer.innerHTML = questionHtml;


//===============================//
//======Button Next Function====//
//=============================//
let buttonNext = document.querySelectorAll(".btn-next");
buttonNext.forEach((button) => {
    button.addEventListener("click", (e) => {
        
        let questiones = e.target.parentElement.parentElement.parentElement
        questiones.setAttribute("style", "display:none;")

        let questionesSibling = questiones.nextElementSibling
        questionesSibling.removeAttribute("style")
    })
})

//===============================//
//===Button Previous Function===//
//=============================//
let buttonPrev = document.querySelectorAll(".btn-prev");
buttonPrev.forEach((button) => {
    button.addEventListener("click", (e) => {
        let choices = e.target.parentElement.parentElement.parentElement
        choices.setAttribute("style", "display:none;")

        let choicesSibling = choices.previousElementSibling
        choicesSibling.removeAttribute("style")
    })
})

//===============================//
//====Button Finish Function====//
//=============================//
let buttonFinish = document.querySelector(".btn-finish");
buttonFinish.addEventListener("click", function() {

        score();
});

//===============================//
//========Looping score=========//
//=============================//
function score() {
    let answerquestion = []
    let checkedquestion = [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    ]
    let openModal = document.querySelector("#my-modal")
    let closeModal = document.querySelector("#close-btn")

    
    for (let i=1; i <= checkedquestion.length; i++){  
        let useranswerquestion = document.querySelector(`input[name=q-${i}]:checked`);
        if (useranswerquestion){
            answerquestion.push(useranswerquestion.value);
        }
    }
    
    if (answerquestion.length < checkedquestion.length) {
        openModal.style.display = "block";
    } else {
        questionContainer.style.display = "none";
        finishContainer.style.display = "flex";
    } 

    closeModal.addEventListener("click", () => {
        openModal.style.display = "none";
    })

    let score = 0;    
    answerquestion.forEach((answers, index) =>{
        
        if(answers === allquestion[index].correctAnswer){
            score +=1;
        }
        
    });

    correctAnswer(score);
}

//===============================//
//========Result Section========//
//=============================//
let userName = document.querySelector(".user-name");

function correctAnswer(score) {
    let resultHTML = "";

    if (score < 7) {
        resultHTML += `
        <h2 class="result-page__person"> ${userName.value}</h2>
        <h3 class="result-page__title">Thanks, for your spirit of learning!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="first">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="first">Unfornately, your score is still below average</p>
        </div>
        <button class="result-page__again btn-again">Again!</button>`
    } else if (score < 10) {
        resultHTML += `
        <h2 class="result-page__person">${userName.value}</h2>
        <h3 class="result-page__title">Thanks, for your spirit of learning!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="second">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="second">Your score is good, keep it up!</p>
        </div>
        <button class="result-page__again btn-again">Again!</button>`
    } else {
        resultHTML += `
        <h2 class="result-page__person"> ${userName.value}</h2>
        <h3 class="result-page__title">Thanks, for your spirit of learning!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="third">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="third">You're Genius!</p>
        </div>
        <button class="result-page__again btn-again">Try Again!</button>`
    } 

    finishContainer.innerHTML = resultHTML;


    let btnAgain = document.querySelector(".btn-again");

    btnAgain.addEventListener("click", function() {
        window.location.reload();
    });
};